var express = require('express');
var mongoose = require('mongoose');
var router = require('./routes/routes');

var server = express();
server.use(express.json());
server.use(router);

mongoose.connect('mongodb://localhost:27017/angular-class', { useNewUrlParser: true }, function dbCallback(error) {
    if (error) {
        console.log("Error");
    } else {
        console.log("DB connected");
    }
});
server.listen(3000, function runFN(error) {
    if (error) {
        console.log("Error");
    } else {
        console.log("Server stated");
    }
});