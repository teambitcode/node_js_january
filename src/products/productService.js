var productModel = require('./productsModel');

module.exports.getAllProducts = function testFN() {

    return new Promise( function (resolve, reject) {
        productModel.find({}, function getData(error, dataValues) {
            if (error) {
                reject(false);
            } else {
                resolve(dataValues);
            }
        });
    });

}


module.exports.createProduct = function testFNCreate(bodyObject) {

    return new Promise( function (resolve, reject) {

        var productModelData = new productModel();
        productModelData.name = bodyObject.name;
        productModelData.description = bodyObject.description;
        productModelData.price = bodyObject.price;

        productModelData.save(function dataSaveRes(error, dataValue) {
            if (error) {
                reject(false);
            } else {
                resolve(true);
            }
        });

    });

}