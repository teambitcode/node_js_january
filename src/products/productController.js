var productService = require('./productService');
var responceService = require('../../services/responceService');

module.exports.myFun = async function testFN(req, res) {

    console.log("getAllProducts before call");
    var allData = await productService.getAllProducts();
    console.log(allData);
    console.log("getAllProducts after call");

    if (allData) {
        responceService.successWithData(res, allData);
    } else {
        responceService.errorWithMessage(res, "Error getting data");
    }
}



module.exports.createProductFn = async function testFN(req, res) {
    console.log(req.body);

    var result = await productService.createProduct(req.body);

    if (result) {
        responceService.successWithData(res, "Data saved");
    } else {
        responceService.errorWithMessage(res, "Error saving data");
    }
}