var mongoose = require('mongoose');
var Schema = mongoose.Schema;

var key = 'abc123abc123abc123abc123abc123abc123';
// Create an encryptor:
var encryptor = require('simple-encryptor')(key);

var userSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    password: {
        type: String,
        required: true
    },
    name: {
        type: String,
        required: true
    }
});

// encrypt the password before save
userSchema.methods.encryptPassword = function (password) {
    const encryptedString = encryptor.encrypt(password);
    return encryptedString;
};

// encrypt the password before save
userSchema.methods.decryptPassword = function (passwordEncrypted) {
    const decryptedString = encryptor.decrypt(passwordEncrypted);
    return decryptedString;
};

module.exports = mongoose.model('users', userSchema);