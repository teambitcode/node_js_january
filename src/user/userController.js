var responceService = require('../../services/responceService');
var userService = require('./userService');

module.exports.createUser = async function testFN(req, res) {
    console.log(req.body);

    var result = await userService.createUser(req.body);

    if (result) {
        responceService.successWithData(res, "User saved");
    } else {
        responceService.errorWithMessage(res, "Error saving user");
    }
}

module.exports.loginUser = async function testFNLogin(req, res) {
    console.log(req.body);

    var result = await userService.loginUser(req.body);

    if (result) {
        responceService.successWithData(res, "Successfully logged");
    } else {
        responceService.errorWithMessage(res, "Error login");
    }
}