var express = require('express');
const router = express.Router();

var productController = require('../src/products/productController');
var userController = require('../src/user/userController');

// user routes
router.route('/user/create')
    .post(userController.createUser);
router.route('/user/login')
    .post(userController.loginUser);

// products routes
router.route('/product/getAll')
    .get(productController.myFun);
router.route('/product/create')
    .post(productController.createProductFn);


module.exports = router;