module.exports.successWithData = (response, allData) => {
    return response.send({
        status: true,
        data: allData
    });
}

module.exports.errorWithMessage = (response, msg) => {
    return response.send({
        status: false,
        message: msg
    });
}